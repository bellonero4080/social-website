const COMMENT_SECTION_TITLE = "Comment Section";
const POSTS_URL = "https://jsonplaceholder.typicode.com/posts";
const COMMENTS_URL = "https://jsonplaceholder.typicode.com/comments";

async function loadDataFromUrl(url) {
    const response = await fetch(url);
    return await response.json();
}

// function loadDataFromUrl(url){
//  return fetch(url)
//   .then(response => response.json());
// }

function showPosts(posts) {
    posts.forEach(post => {
        const pageContainer = document.getElementsByClassName("container")[0];
        const postArticle = document.createElement("article");
        postArticle.classList.add("post-article");

        const postTitle = document.createElement("h2");
        postTitle.classList.add("post-title");
        postTitle.innerText = post.title;
        postArticle.appendChild(postTitle);

        const postContent = document.createElement("p");
        postContent.innerText = post.body;
        postArticle.appendChild(postContent);

        const commentSection = document.createElement("section");
        commentSection.setAttribute('id', `comment-section-${post.id}`);
        postArticle.appendChild(commentSection);

        const commentSectionTitle = document.createElement("h3");
        commentSectionTitle.innerText = COMMENT_SECTION_TITLE;
        commentSection.appendChild(commentSectionTitle);

        pageContainer.appendChild(postArticle);
    })
}

function showComments(comments) {
    comments.forEach(comment => {
        const commentTitle = document.createElement("h4");
        commentTitle.innerText = comment.name;

        const commentContent = document.createElement("p");
        commentContent.innerText = comment.body;

        const commentDivision = document.createElement("hr");
        const postCommentSection = document.getElementById(`comment-section-${comment.postId}`)

        postCommentSection.appendChild(commentDivision);
        postCommentSection.appendChild(commentTitle);
        postCommentSection.appendChild(commentContent);
    })
}

window.onload = async () => {
    const posts = await loadDataFromUrl(POSTS_URL);
    const comments = await loadDataFromUrl(COMMENTS_URL);

    showPosts(posts);
    showComments(comments);
}

// window.onload = () => {
//   loadDataFromUrl(POSTS_URL).then(posts => showPosts(posts));
//   loadDataFromUrl(COMMENTS_URL).then(comments => showComments(comments));
// }